from django.shortcuts import render

# Create your views here.
from django.views.generic import FormView, CreateView, View
from django import forms
from .models import PrivateAttachment
from django.http import HttpResponse


class UploadForm(forms.ModelForm):
    class Meta:
        model = PrivateAttachment
        fields = (
            'file',
        )


class UploadView(CreateView):
    form_class = UploadForm
    template_name = 'upload.html'
    model = PrivateAttachment
    success_url = '/upload/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['files'] = self.model.objects.all()
        return context

    # def form_valid(self, form):
    #     form.save()
    #     return HttpResponse()


class FileView(View):
    def get(self, request, *args, **kwargs):
        from urllib.parse import urlparse

        attach = PrivateAttachment.objects.get(pk=self.kwargs['file_id'])

        o = urlparse(attach.file.url)
        print(o.scheme, o.netloc, o.path)
        # o = PrivateAttachment.objects.first()
        # print(o.file.url)
        response = HttpResponse()
        x_accel_redirect = f'/minio/{o.scheme}/{o.netloc}{o.path}'
        print(x_accel_redirect)
        response['X-Accel-Redirect'] = x_accel_redirect
        return response
